import app from './app';
import {config} from 'dotenv';

config({path: `${process.cwd()}/.env`});
const PORT = process.env.APP_PORT || 9500;


try {
  app.listen(PORT, () => {
    console.log(`😎  😁   Server is running at port ${PORT}   😁  😎`);
  });
} catch (err) {
  console.log(err);
};
