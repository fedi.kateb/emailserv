import express from 'express';
import cnx from './infrastucture/connectMongo';

import {routes} from './api/routes/sendemail';
import bodyParser from 'body-parser';

const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

cnx.initDB(app);
routes(app);
export default app;
