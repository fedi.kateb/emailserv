import sendEmail from '../actions/sendemailhandler';
export const routes = (app) => {
  app.route('/api/emailsend')
      .post(sendEmail);
};
