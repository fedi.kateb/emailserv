import nodemailer from 'nodemailer';
import verifyTemplate from '../templates/verifytemplate';
import {config} from 'dotenv';

config({path: `${process.cwd()}/.env`});
  const emailUser = process.env.EMAIL_USER;
  const emailPass= process.env.EMAIL_PASS;
export const sendemail = async (user, action) => {
  const email = user.login;

  const emailDispatcher = {
    verifyEmail: verifyTemplate,

  };
  const template = emailDispatcher[action](user, action);
  const testAccount = await nodemailer.createTestAccount();
  console.log(testAccount);
  // create reusable transporter object using the default SMTP transport
  const transporter = await nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: emailUser, // generated ethereal user
      pass: emailPass, // generated ethereal password
    },
  });

  // send mail with defined transport object
  const info = await transporter.sendMail({
    from: 'kateb.fedy@esprit.tn', // sender address
    // eslint-disable-next-line max-len
    to: `${email}`, // list of receivers
    subject: action, // Subject line
    text: template.text, // plain text body
    html: template.html, // html body
  });
  console.log( info);
  console.log('Message sent: %s', info.messageId );
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
};
