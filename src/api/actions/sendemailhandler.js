
import {sendemail} from '../domains/emailService';

const sendEmail = async (req, res) => {
  const {body: {user, action}} = req;

  if (!user || !action ) {
    res.status(400).send({msg: 'Invalid request'});
  }
  const result = await sendemail({user, action});
  res.send(result);
};

export default sendEmail;
